package com.epam.rd.java.basic.task8.controller;

import java.util.Comparator;

public class Flower implements Comparator {
    String name;
    String soil;
    String origin;
    //visualParameters
    String stemColour;
    String leafColour;
    String aveLenFlowerMeasure;
    String aveLenFlower;
    //growingTips
    String tempretureMeasure;
    String tempreture;
    String lightRequiring;
    String wateringMeasure;
    String watering;
    //
    String multiplying;

    @Override
    public int compare(Object o1, Object o2) {
        Flower f1 = (Flower)o1;
        Flower f2 = (Flower)o2;
        return f1.name.compareTo(f2.name);
    }

    @Override
    public String toString() {
        return "Flower{" +
                "name='" + name + '\'' +
                ", soil='" + soil + '\'' +
                ", origin='" + origin + '\'' +
                ", stemColour='" + stemColour + '\'' +
                ", leafColour='" + leafColour + '\'' +
                ", aveLenFlowerMeasure='" + aveLenFlowerMeasure + '\'' +
                ", aveLenFlower='" + aveLenFlower + '\'' +
                ", tempretureMeasure='" + tempretureMeasure + '\'' +
                ", tempreture='" + tempreture + '\'' +
                ", lightRequiring='" + lightRequiring + '\'' +
                ", wateringMeasure='" + wateringMeasure + '\'' +
                ", watering='" + watering + '\'' +
                ", multiplying='" + multiplying + '\'' +
                '}';
    }
}
