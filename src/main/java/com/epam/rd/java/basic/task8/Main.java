package com.epam.rd.java.basic.task8;

import com.epam.rd.java.basic.task8.controller.*;

import java.io.File;
import java.util.ArrayList;
import java.util.List;

public class Main {
	
	public static void main(String[] args) throws Exception {
		if (args.length != 1) {
			return;
		}

		String xmlFileName = args[0];
		System.out.println("Input ==> " + xmlFileName);
		
		////////////////////////////////////////////////////////
		// DOM
		////////////////////////////////////////////////////////
		
		// get container
		DOMController domController = new DOMController(xmlFileName);
		// PLACE YOUR CODE HERE
		List<Flower> domList=domController.readXML();

		// sort (case 1)
		domList.sort(new Flower());
		System.out.println(domList);
		// save
		String outputXmlFile = "output.dom.xml";
		// PLACE YOUR CODE HERE
		domController.writeXML(outputXmlFile,domList);
		////////////////////////////////////////////////////////
		// SAX
		////////////////////////////////////////////////////////
		
		// get
		SAXController saxController = new SAXController(xmlFileName);
		// PLACE YOUR CODE HERE
		List<Flower> list = new ArrayList<>();
		list = saxController.readXML();
		System.out.println(list);
		// sort  (case 2)
		// PLACE YOUR CODE HERE
		list.sort(new Flower());
		System.out.println(list);
		// save
		outputXmlFile = "output.sax.xml";
		// PLACE YOUR CODE HERE
		saxController.writeXML(outputXmlFile,list);
		////////////////////////////////////////////////////////
		// StAX
		////////////////////////////////////////////////////////
		
		// get
		STAXController staxController = new STAXController(xmlFileName);
		List<Flower> staxList = staxController.readXML();
		System.out.println("STAX");
		System.out.println(staxList);
		// sort  (case 3)
		staxList.sort(new Flower());
		// save
		outputXmlFile = "output.stax.xml";
		// PLACE YOUR CODE HERE
		staxController.writeXML(outputXmlFile,staxList);
	}

}
