package com.epam.rd.java.basic.task8.controller;

import org.xml.sax.helpers.DefaultHandler;
import org.xml.sax.Attributes;
import org.xml.sax.SAXException;
import org.xml.sax.helpers.DefaultHandler;

import javax.xml.parsers.ParserConfigurationException;
import javax.xml.parsers.SAXParser;
import javax.xml.parsers.SAXParserFactory;
import javax.xml.stream.XMLOutputFactory;
import javax.xml.stream.XMLStreamException;
import javax.xml.stream.XMLStreamWriter;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

/**
 * Controller for SAX parser.
 */
public class SAXController extends DefaultHandler {
	
	private String xmlFileName;
	private static List<Flower> list = new ArrayList<>();
	public SAXController(String xmlFileName) {
		this.xmlFileName = xmlFileName;
	}
	public void writeXML(String nameFile,List<Flower> list) throws FileNotFoundException, XMLStreamException {
		XMLOutputFactory factory = XMLOutputFactory.newInstance();
		XMLStreamWriter writer = factory.createXMLStreamWriter(new FileOutputStream(nameFile));

		writer.writeStartDocument("UTF-8","1.0");
		//writer.writeAttribute("standalone","yes");
		writer.writeStartElement("flowers");
		writer.writeAttribute("xmlns","http://www.nure.ua");
		writer.writeAttribute("xmlns:xsi","http://www.w3.org/2001/XMLSchema-instance");
		writer.writeAttribute("xsi:schemaLocation","http://www.nure.ua input.xsd ");

		for(Flower flower : list){
			writer.writeStartElement("flower");
			writer.writeStartElement("name");
			writer.writeCharacters(flower.name);
			writer.writeEndElement();
			writer.writeStartElement("soil");
			writer.writeCharacters(flower.soil);
			writer.writeEndElement();
			writer.writeStartElement("origin");
			writer.writeCharacters(flower.origin);
			writer.writeEndElement();
			writer.writeStartElement("visualParameters");
			writer.writeStartElement("stemColour");
			writer.writeCharacters(flower.stemColour);
			writer.writeEndElement();
			writer.writeStartElement("leafColour");
			writer.writeCharacters(flower.leafColour);
			writer.writeEndElement();
			writer.writeStartElement("aveLenFlower");
			writer.writeAttribute("measure",flower.aveLenFlowerMeasure);
			writer.writeCharacters(flower.aveLenFlower);
			writer.writeEndElement();
			writer.writeEndElement();
			writer.writeStartElement("growingTips");
			writer.writeStartElement("tempreture");
			writer.writeAttribute("measure",flower.tempretureMeasure);
			writer.writeCharacters(flower.tempreture);
			writer.writeEndElement();

			writer.writeStartElement("lighting");
			writer.writeAttribute("lightRequiring","yes");
			writer.writeEndElement();
			writer.writeStartElement("watering");
			writer.writeAttribute("measure",flower.wateringMeasure);
			writer.writeCharacters(flower.watering);
			writer.writeEndElement();

			writer.writeEndElement();
			//writer.writeEndElement();
			writer.writeStartElement("multiplying");
			writer.writeCharacters(flower.multiplying);

			writer.writeEndElement();
			writer.writeEndElement();
		}

		writer.writeEndElement();

		writer.writeEndDocument();
	}
	public List<Flower> readXML()throws ParserConfigurationException, SAXException, IOException {

		// Создание фабрики и образца парсера
		SAXParserFactory factory = SAXParserFactory.newInstance();
		SAXParser parser = factory.newSAXParser();


		XMLHandler handler = new XMLHandler();
	//	File file = new File("C:\\Users\\STASON\\IdeaProjects\\task-08\\output.sax.xml");
		parser.parse(new File("C:\\Users\\STASON\\IdeaProjects\\task-08\\input.xml"), handler);

		return list;
	}
	private static class XMLHandler extends DefaultHandler {
		private String lastElement,name,soil,origin,stemColour,leafColor,aveLenFlowerMeasure,aveLenFlower,tempretureMeasure,tempreture,lightRequiring,wateringMeasure,watering,multiplying;

		@Override
		public void startElement(String uri, String localName, String qName, Attributes attributes) throws SAXException {
			lastElement = qName;
			if(lastElement.equals("aveLenFlower")){
				aveLenFlowerMeasure = attributes.getValue("measure");
			}
			if(lastElement.equals("tempreture")){
				tempretureMeasure= attributes.getValue("measure");
			}
			if(lastElement.equals("lighting")){
				lightRequiring = attributes.getValue("lightRequiring");
			}
			if(lastElement.equals("watering")){
				wateringMeasure = attributes.getValue("measure");
			}

		}

		@Override
		public void characters(char[] ch, int start, int length) throws SAXException {
			String information = new String(ch, start, length);
			information = information.replace("\n", "").trim();

			if(!information.isEmpty()) {
				switch (lastElement) {
					case "name":
						name = information;
						break;
					case "soil":
						soil = information;
						break;
					case "origin":
						origin = information;
						break;
					case "stemColour":
						stemColour = information;
						break;
					case "leafColour":
						leafColor = information;
						break;
					case "aveLenFlower":
						aveLenFlower = information;
						break;
					case "tempreture":
						tempreture = information;
						break;
					case "watering":
						watering = information;
						break;
					case "multiplying":
						multiplying = information;
						break;
				}
			}
		}

		@Override
		public void endElement(String uri, String localName, String qName) throws SAXException {
			if(qName.equals("flower")){
				Flower flower = new Flower();
				flower.aveLenFlower=aveLenFlower;
				flower.leafColour=leafColor;
				flower.stemColour=stemColour;
				flower.origin=origin;
				flower.soil=soil;
				flower.name=name;
				flower.lightRequiring=lightRequiring;
				flower.multiplying=multiplying;
				flower.tempreture=tempreture;
				flower.tempretureMeasure=tempretureMeasure;
				flower.aveLenFlowerMeasure=aveLenFlowerMeasure;
				flower.watering=watering;
				flower.wateringMeasure=wateringMeasure;
				list.add(flower);
			}
		}
	}
}